<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Title Page</title>
		<style media="screen">
			body{
				background-color:white;
			}
		</style>

	</head>
	<body>
		<h1 class="text-center">OOP</h1>



<?php

class Car{

		 public $comp;
		 public $color;
		 public $hasSunRoof=True;
		 public $tank;
		 public $model;

		 public function hello()
		 {
		 	return "Beep i am " . $this -> comp . "<br />";
		 }

		 public function fill($float)
		 {
		 	$this -> tank +=$float;
			return $this;
		 }

		 public function ride($float)
		 {
		 	$miles = $float;
			$gallons = $miles/50;
			$this-> tank -= $gallons;
			return $this;
		 }

		 //this can be also called getter and setter method
			public function getModel($string)
			{
				$this-> model = $string;
				return "The car model is ". $this -> model . "<br />";
			}

}

$BMW = new Car();
$BMW -> comp = "Bmw <br />";
echo $BMW -> comp;
echo $BMW -> hello();
echo $BMW -> getModel("toyota");
//Chaining methods and properties
$tank = $BMW -> ride(50) -> fill(200) -> tank;

echo "The number of gallons left in the tank" . $tank . "gal";
echo "<br/>";



class User{

	public $firstName;
	private $lastName;

	public function hello()
	{
	 return "i am " . $this -> firstName . "<br />";
	}

//setter method
	public function setName($lastName)
	{
		$this-> lastName = $lastName;

	}
//getter method
	public function getName()
	{
	 return "i am " . $this -> lastName;
	}



	public function getsetName($string)
	{
		$allowedModels = array("foo", "bar", "hello");

		if(in_array($string,$allowedModels))
			{
				$this-> lastName = $string;
			}
			return "i amn " . $this -> lastName . "<br />";

		//$this-> lastName = $string;
		//return "i am " . $this -> lastName . "<br />";
	}
}

//in this way only public property can be accessed but not private
$user = new User();
$user -> firstName = "Joe ";
echo $user -> hello();

//accessing private value using setter and getter
$user -> setName("Minhaj");
echo $user -> getName();

//getter setter combination
echo $user -> getsetName("foo");

 ?>


<?php
 echo $BMW -> getModel("toyota");
?>


	</body>
</html>
