<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Title Page</title>
		<style media="screen">
			body{
				background-color:white;
			}
		</style>

	</head>
	<body>
		<h1 class="text-center">Constructor</h1>



<?php
class Car {
		private $model='';

		// A constructor(called Magic method) method which looks alike setter method except it need a default value.
		//a constructor is to set the values of properties as soon as we create objects out of classes.
		public function __construct($model=Null)
			{
				if ($model)
				{
					$this -> model = $model;
				}

			}
		public function getCarModel()
			{
				//__CLASS__ it is called Magic constant
				//Other magic constants that may be of help are:
				//__LINE__ to get the line number in which the constant is used.
				//__FILE__ to get the full path or the filename in which the constant is used.
				//__METHOD__ to get the name of the method in which the constant is used.
			return ' The '.__CLASS__.' model is: ' . $this -> model . ' and line no is:'. __LINE__;
			}

	}

$car1 = new Car("BMW");

echo $car1 -> getCarModel();

?>

	</body>
</html>
