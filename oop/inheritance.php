<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Title Page</title>
		<style media="screen">
			body{
				background-color:white;
			}
		</style>

	</head>
	<body>
		<h1 class="text-center">Inheritance</h1>



<?php
//We use inheritance in order to reduce code duplication by using
//code from the parent class in the child classes.
class Car {
		//it can only accessible from parent class
		private $model='';
		public $comp;

		//it can be accessible fro both parent and child class
		protected $color;
		public $hasSunRoof=True;


		public function hello()
				{
				return "beep";
				}

				// with final keyword means it cant be overriden
		final public function __construct($model=Null)
			{
				if ($model)
				{
					$this -> model = $model;
				}

			}
		public function getCarModel()
			{
			return $this -> model;
			}

	}

$car1 = new Car();
echo $car1 -> getCarModel();


// The child class inherits the code
// from the parent class and with his own property and method
class SportsCar extends Car {

	private $style="furious";

	public function driveWithStyle()
	{
		return "Hi, i am " . $this-> getCarModel() ." and i am an extended " . $this -> style." ".__CLASS__;
	}

	//Override method of parent class
	//but if parent has a "final" keyword we cant override them.
	public function hello()
		{
		return "beep!!!" . "i am ". $this -> getCarModel();
		}


}
// Create an instance from the child class
$sportsCar1 = new SportsCar("Bugatti");
echo $sportsCar1 -> driveWithStyle() . "<br />";
echo $sportsCar1 -> hello();







?>

	</body>
</html>
