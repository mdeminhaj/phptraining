<?php
class User{

	public $firstName;

  //private property can not be accessed from outside of class scope.
  //So, it can only be accessed using getter and setter method
	private $lastName;

	public function hello()
	{
	 return "i am " . $this -> firstName . "<br />";
	}

//setter method
	public function setName($lastName)
	{
		$this-> lastName = $lastName;

	}
//getter method
	public function getName()
	{
	 return "i am " . $this -> lastName;
	}


//combination of getter and setter
	public function getsetName($string)
	{
		$this-> lastName = $string;
		return "i am " . $this -> lastName . "<br />";
	}



	public function getsetAllowed($string)
	{
		$allowedNames = array("foo", "bar", "hello");

		if(in_array($string,$allowedNames))
			{
				$this-> lastName = $string;
			}
			return "i amn " . $this -> lastName . "<br />";

		//$this-> lastName = $string;
		//return "i am " . $this -> lastName . "<br />";
	}
}

//in this way only public property can be accessed but not private
$user = new User();
$user -> firstName = "Joe ";
echo $user -> hello();

//accessing private value using setter and getter
$user -> setName("Minhaj");
echo $user -> getName();

//getter setter combination
echo $user -> getsetName("jh");

echo $user -> getsetAllowed("bar");

?>
