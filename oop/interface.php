<?php
/*
Interfaces vs Abstract Classes
Interfaces and abstract classes are pretty similar. They both help create templates for classes that need them. So, which one do you use and what are the major differences? Here a couple of things to know about each one.

1.Methods in abstract classes can be defined abstract or just a regular method. If a method is abstract, then that method must be defined in the class that extends it. In interfaces, ALL methods are abstract, therefore all methods must be defined in the class that implements them.
2.You’re allowed to define your methods as public, private, or protected in abstract classes. In interfaces, ALL methods MUST be public.
3.In abstract classes, you’re allowed to create methods and properties with values and concrete code in them as long as they’re not defined as abstract. Interfaces CAN NOT contain properties and methods CAN NOT have a body.
4.A class can only extend 1 abstract class, but a class can implement an infinite amount of interfaces even if the interfaces don’t relate to one another.
5.You have the option to override methods in an abstract class, but you MUST override ALL methods in an interface.

As I stated earlier, you can have a class inherit multiple interfaces. To do this, you separate each interface with a comma.


*/


 ?>
