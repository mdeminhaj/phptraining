1.git status
2.Make your changes
3.git add <any changed files>
4.git commit -m <Commit comment>
5.git push origin master



1. git add -u :/ adds all modified file changes to the stage 
2. git add * :/ adds modified and any new files (that's not gitignore'ed) to the stage

----
git status
Shows state of the repository. Should show hello.py isn't tracked
git add hello.py
Add file to list of files to be stored in the repository
git status
Show file is now added
git commit -m "first commit"
Save the file in the version of the repository on cloud9 (not on bitbucket though which is where we really want it saved eventually)
git status
File is not
git push --set-upstream origin master
type 'yes' to confirm connection to bitbucket at the scary looking 'are you sure you want to connect?' message
This is a 'Magic' command and does this:
pushes the files in the cloud9 version of the repository to the bitbucket version of the repository
'origin master' tells the git the exact details of the repository it needs
the '--set-upstream' bit means you never need to add the 'origin master' bit ever again :-)