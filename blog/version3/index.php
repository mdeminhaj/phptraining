	<?php 	
	error_reporting(E_ALL ^ E_NOTICE);


	 ?>





<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Simple Blog</title>

		<!-- Bootstrap CSS -->
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<h1 class="text-center">My Functional Blog</h1>

		<?php 

		include_once("connection.php");

		$sql = "SELECT * FROM blogpost ORDER BY id DESC";
		$result = mysqli_query($dbCon, $sql);

		while ($row = mysqli_fetch_array($result)) {
			$title = $row['title'];
			$subtitle = $row['subtitle'];
			$content = $row['content'];

		?>
			<h2><?php echo $title; ?> - <small><?php echo $subtitle;?></small></h2>
			<p><?php echo $content; ?></p>
			<hr />
		<?php
				}
		 ?>

		


		<a href="admin.php">Admin</a>
		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	</body>
</html>