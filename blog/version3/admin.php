<?php 	
	error_reporting(E_ALL ^ E_NOTICE);
	session_start();

	if (isset($_SESSION['username'])) {
		//$useId = $_SESSION['id'];
		$username = ucfirst($_SESSION['username']);
	
	
	if ($_POST['submit']) {
		$title = $_POST['title'];
		$subtitle = $_POST['subtitle'];
		$content = $_POST['content'];

		include_once("connection.php");

		$sql =  "INSERT INTO blogpost(title, subtitle, content) VALUE ('$title','$subtitle','$content')";
		mysqli_query($dbCon,$sql);
		echo "Blog Entry Posted";
		
		} 

	} else {
			
			header('Location: login.php');
			die();
		}
	


	 ?>



<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Title Page</title>

		<!-- Bootstrap CSS -->
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<h1 class="text-center">Hello <?php echo $username; ?>!</h1>


		<div class="container">


		<form action="admin.php" method="POST" role="form">
			<legend>Create New Post</legend>
		
			<div class="form-group">
				<label for="">Title</label>
				<input type="text" name="title" class="form-control" id="" placeholder="Input field">
			</div>
			<div class="form-group">
				<label for="">Subtitle</label>
				<input type="text" name="subtitle" class="form-control" id="" placeholder="Input field">
			</div>
			<div class="form-group">
				<label for="">Content</label>
				<input type="text" name="content" class="form-control" id="" placeholder="Input field">
			</div>
	
			<button type="submit" value="submit" name="submit" class="btn btn-primary">submit</button>
		</form>

			
		</div>
		
		<br />
		<br />
		<div class="container">

		<a href="index.php">View Home Page</a>
			
		</div>
		<br />
		<div class="container">
			<form action="logout.php" method="POST" role="form">
			<legend>Logout</legend>
	
			<button type="submit" class="btn btn-primary" value="logout">Logout</button>
		</form>
		</div>

		
		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	</body>
</html>