<?php
// including the database connection file
include_once("connection.php");
 
if(isset($_POST['update']))
{	
	$id = $_POST['id'];
	
	$title=$_POST['title'];
	$subtitle=$_POST['subtitle'];
	$content=$_POST['content'];	
	
	// checking empty fields
	if(empty($title) || empty($subtitle) || empty($content)) {	
			
		if(empty($title)) {
			echo "<font color='red'>Name field is empty.</font><br/>";
		}
		
		if(empty($subtitle)) {
			echo "<font color='red'>Age field is empty.</font><br/>";
		}
		
		if(empty($content)) {
			echo "<font color='red'>Email field is empty.</font><br/>";
		}		
	} else {	
		//updating the table
		$sql = "UPDATE blogpost SET title='$title',subtitle='$subtitle',content='$content' WHERE id=$id";
		$result = mysqli_query($dbCon, $sql);
		
		//redirectig to the display page. In our case, it is index.php
		header("Location: index.php");
	}
}
?>

<?php
//getting id from url
//Local variable
$id = $_GET['id'];
 
//selecting data associated with this particular id
$sql = "SELECT * FROM blogpost WHERE id=$id";
$result = mysqli_query($dbCon, $sql);

 
while($res = mysqli_fetch_array($result))
{
	$title = $res['title'];
	$subtitle = $res['subtitle'];
	$content = $res['content'];
}
?>



<html>
<head>	
	<title>Edit Data</title>
</head>
 
<body>
	<a href="index.php">Home</a>
	<br/><br/>
	
	<form name="form1" method="post" action="edit.php">
		<table border="0">
			<tr> 
				<td>Title</td>
				<td><input type="text" name="title" value=<?php echo $title;?>></td>
			</tr>
			<tr> 
				<td>Subtitle</td>
				<td><input type="text" name="subtitle" value=<?php echo $subtitle;?>></td>
			</tr>
			<tr> 
				<td>Content</td>
				<td><textarea name="content"><?php echo $content;?></textarea></td>
			</tr>
			<tr>
				<td><input type="hidden" name="id" value=<?php echo $_GET['id'];?>></td>
				<td><input type="submit" name="update" value="Update"></td>
			</tr>
		</table>
	</form>
</body>
</html>