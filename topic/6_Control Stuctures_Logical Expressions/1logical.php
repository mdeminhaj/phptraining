<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
	<head>
		<title>Logical</title>
	</head>
	<body>
		
		<?php 

/*		if (expression) {

			code to execute if the above expression is TRUE

		} elseif (different expression) {

			code to execute if first expression is FALSE

			but the ELSEIF expression is TRUE 

		} else {

				Code to execute if neither

				of the above expressions are TRUE 
		}
*/
		 ?>

		

		<?php
			$a = 3;
			$b = 4;
			
			if ($a > $b) {
				echo "a is larger than b";
			} elseif ($a < $b) {
				echo "a is smaller than b";
			} else {
				echo "a is equal to b";
			}

		
		?>
		

		<br />
		<?php // Only welcome new users
			$new_user = true;
			if ($new_user) {
				echo "<h1>Welcome!</h1>";
				echo "<p>We are glad you decided to join us.</p>";
			}
		?>


		<br />
		
		<?php // don't divide by zero
			$numerator = 20;
			$denominator = 5;
			$result = 0;
			if ($denominator > 0) {
				$result = $numerator / $denominator;
			}
			echo "Result: $result";
		?>
		
		


		<!-- Operator Example: -->
		
		<br />
		<?php 

			$native_language = "Spanish";

			if ($native_language == "French") {

			echo "Bonjour! Vouz parlez Français.";

			} elseif ($native_language == "Spanish") {

			echo "¡Hola! Usted habla Español.";

			} else {

			echo "Hello! You probably speak English.";

			}

		 ?>

		<br />
		 <?php 

		 	$username = "johnny boy";

			$password = "qwerty";

			if ($username == 'johnny boy' && $password == 'qwerty') {

				echo "welcome $username"; 

			} else {

				echo "Your username and password combination are incorrect";

			}

		  ?>


			<br />
		  <?php

				$birthYear = 1989;

				$thisYear = date('Y');

				$myAge = ($thisYear - 1988);

				echo $myAge;
			?>

			


	</body>
</html>
