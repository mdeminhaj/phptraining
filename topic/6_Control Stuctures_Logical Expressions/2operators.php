<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
	<head>
		<title>Comparison and Logical Operators</title>
	</head>
	<body>

		<?php
			$a = 4;
			$b = 3;
			$c = 1;
			$d = 20;
			if (($a >= $b) || ($c >= $d)) {
				echo "a is larger than b OR ";
				echo "c is larger than d";
			}
		?>
		<br />
		<?php
			$e = 100;
			if (!isset($e)) {
				$e = 200;
			}
			echo $e;
		?>
		<br />
		<?php
			// don't reject 0 or 0.0
			$quantity = "";
			if (empty($quantity) && !is_numeric($quantity)) {
				echo "You must enter a quantity.";
			}
		?>

    <?php

    //Alternative syntax for control structures
    /* Correct Method: */
    if($a > $b):
        echo $a." is greater than ".$b;
    elseif($a == $b): // Note the combination of the words.
        echo $a." equals ".$b;
    else:
        echo $a." is neither greater than or equal to ".$b;
    endif;


     ?>

     <p>
       PHP offers an alternative syntax for some of its control structures;
       namely, if, while, for, foreach, and switch.
       In each case, the basic form of the alternate syntax is to change the opening brace to a colon (:)
       and the closing brace to endif;, endwhile;, endfor;, endforeach;, or endswitch;, respectively.
     </p>

     <?php if ($a == 5): ?>
       A is equal to 5
     <?php endif; ?>



	</body>
</html>
