<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
	<head>
		<title>Loops: for</title>
	</head>
	<body>

		<?php // while loop example
		  $count = 0;
		  while ($count <= 10) {
		    echo $count . ", ";
		    $count++;
		  }
		?>
		<br />


  <?php
    /* example 1 */

    for ($i = 1; $i <= 10; $i++) {
        echo $i;
    }

    /* example 2 */

    for ($i = 1; ; $i++) {
        if ($i > 10) {
            break;
        }
        echo $i;
    }

    /* example 3 */

    $i = 1;
    for (; ; ) {
        if ($i > 10) {
            break;
        }
        echo $i;
        $i++;
    }

    /* example 4 */

    for ($i = 1, $j = 0; $i <= 10; $j += $i, print $i, $i++);


     ?>


<br />



		<?php

			for($count = 0; $count <= 10; $count++) {
		    echo $count . ", ";
			}

		?>

		<br />
		<?php

			for ($count = 20; $count > 0; $count--) {
				if ($count % 2 == 0) {
					echo "{$count} is even.<br />";
				} else {
					echo "{$count} is odd.<br />";
				}
		  }

		?>


    <?php
/*
 * This is an array with some data we want to modify
 * when running through the for loop.
 */
$people = array(
    array('name' => 'Kalle', 'salt' => 856412),
    array('name' => 'Pierre', 'salt' => 215863)
);

for($i = 0; $i < count($people); ++$i) {
    $people[$i]['salt'] = mt_rand(000000, 999999);
}
?>


<?php
$people = array(
    array('name' => 'Kalle', 'salt' => 856412),
    array('name' => 'Pierre', 'salt' => 215863)
);

for($i = 0, $size = count($people); $i < $size; ++$i) {
    $people[$i]['salt'] = mt_rand(000000, 999999);
}
?> 


	</body>
</html>
