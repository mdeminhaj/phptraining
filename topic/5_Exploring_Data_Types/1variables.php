<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
	<head>
		<title>Variables</title>
	</head>
	<body>
	<?php

	//Variable names are case-sensitive
	//Variable names must start with a letter of the alphabet or the  _  (underscore) character.
	//Variable names can contain only the characters a-z, A-Z, 0-9, and _(underscore).
	/*Variable names may not contain spaces. If a variable must comprise more than
	one word, it should be separated with the  _  (underscore)  character (e.g., $user_name).*/

  //Note: $this is a special variable that can't be assigned


	$var1 = 10;
	echo $var1;

	echo "<br />";

	$var1 = 100;
	echo $var1;

	echo "<br />";

	$var2 = "Hello world";
	echo $var2;



  //PHP also offers another way to assign values to variables, assign by reference
  //To assign by reference, simply prepend an ampersand (&) to the beginning of the variable which is being assigned (the source variable).
  $foo = 'Boby';              // Assign the value 'Bob' to $foo
  $bar = &$foo;              // Reference $foo via $bar.
  $bar = "My name is $bar";  // Alter $bar...
  echo $bar;
  echo $foo;                 // $foo is altered too.



//variable scope
$a = 1; //  global scope


$b = 2;

function Summ()
{
    global $a, $b; //local scope. By using global keyword it can use global variable

    $b = $a + $b;
}

Summ();
echo $b;


//A second way to access variables from the global scope is to use the special PHP-defined $GLOBALS array
//$GLOBALS exists in any scope, this is because $GLOBALS is a superglobal
$a = 1;
$b = 2;

function Sum()
{
    $GLOBALS['b'] = $GLOBALS['a'] + $GLOBALS['b'];
}

Sum();
echo $b;





//Using static variables
echo "<br />";

function test()
{
    static $a = 0;//without static it will print 0 everytime its being called
    echo $a;
    $a++;
}

test();
test();
test();

echo "<br />";

//Static variables with recursive functions
//A recursive function is one which calls itself


function test2()
{
    static $count = 0;

    $count++;
    echo $count;
    if ($count < 10) {
        test2();
    }

}

test2();



	?>
	</body>
</html>
