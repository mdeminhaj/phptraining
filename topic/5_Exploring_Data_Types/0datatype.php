<?php
/*
PHP supports eight primitive types.

Four scalar types:
  ◦boolean
  ◦integer
  ◦float (floating-point number, aka double)
  ◦string

Two compound types:

  ◦array
  ◦object
And finally two special types:

  ◦resource
  ◦NULL
This manual also introduces some pseudo-types for readability reasons:

  ◦mixed
  ◦number
  ◦callback (aka callable)
  ◦array|object
  ◦void
And the pseudo-variable $...

Some references to the type "double" may remain in the manual. Consider double the same as float; the two names exist only for historic reasons.
*/



$a_bool = TRUE;   // a boolean
$a_str  = "foo";  // a string
$a_str2 = 'foo';  // a string
$an_int = 12;     // an integer


//To get a human-readable representation of a type for debugging, use the gettype() function.
echo gettype($a_bool); // prints out:  boolean
echo gettype($a_str);  // prints out:  string


//To check for a certain type, do not use gettype(), but rather the is_type functions.
// If this is an integer, increment it by four
if (is_int($an_int)) {
    $an_int += 4;
}

// If $a_bool is a string, print it out
// (does not print out anything)
if (is_string($a_bool)) {
    echo "String: $a_bool";
}


//To check the type and value of an expression, use the var_dump() function.
$b = 3.1;
$c = true;
var_dump($b, $c);


?>
