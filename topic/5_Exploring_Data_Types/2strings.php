<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
	<head>
		<title>Strings</title>
	</head>
	<body>


	<?php



	echo "Hello World1<br />";
	echo 'Hello World2<br />';

	$greeting = "Hello";
	$target = "World";
	$phrase = $greeting . " " . $target;
	echo $phrase;
	?>
	<br />
  <h6>Next</h6>
	<?php

	echo "$phrase Again1<br />";
	echo '$phrase Again<br />';
	echo "{$phrase}Again<br />";


	//variable substitution:

	$info = 'Preface variables with a $ like this: $variable';

	/*In this case, every character within the single-quoted string is assigned to  $info. If
	you had used double quotes, PHP would have attempted to evaluate  $variableas a
	variable.
	On the other hand, when you want to include the value of a variable inside a string,
	you do so by using double-quoted strings:*/
	$count = 2;
	echo "This week $count people have viewed your profile";


	//Escaping characters:

	$text = "She wrote upon it, \"Return to sender\".";
	echo $text;


	/*Additionally, you can use escape characters to insert various special characters into
strings such as tabs, newlines, and carriage returns. These are represented, as you
might  guess,by \t,  \n, and  \r. These are used for html outputting and for presenting directory
*/

//print "<font size=\"12\" color=\"#000000\">I said, \"Hey!\"<br />";



URL: http://able2know.org/topic/71607-1

//you will see this in the source code

	$heading = "Date\tName\n Payment";
	echo $heading;

//If you need to send plain text to your browser, you can use something like:
//A file instead of a browser output. Thats it!


//header('Content-type: text/plain');






	/*These special backslash-preceded characters work only in double-quoted strings.
	Within single-quoted strings, only the escaped apostrophe
	(\') and escaped backslash itself (\\) are recognized as escaped characters.*/


	//nl2br — Inserts HTML line breaks before all newlines in a string
	echo nl2br("foo isn't\n bar");
	echo nl2br("Welcome\r\nThis is my HTML document", false);
	$string = "This\r\nis\n\ra\nstring\r";
	echo nl2br($string);

	/*because each OS have different ASCII chars for linebreak:
	windows = \r\n
	unix = \n
	mac = \r

	" " (ASCII 32 (0x20)), an ordinary space.
	"\t" (ASCII 9 (0x09)), a tab.
	"\n" (ASCII 10 (0x0A)), a new line (line feed).
	"\r" (ASCII 13 (0x0D)), a carriage return.
	"\0" (ASCII 0 (0x00)), the NUL-byte.
	"\x0B" (ASCII 11 (0x0B)), a vertical tab.





	*/


	?>


	<br />
			<?php
			echo 'This is my string.\n';
			echo '<br />';
			echo "This is my string\ndfdfdf";
			?>


<!-- TRIM Example: -->

	<?php

	$text   = "\t\tThese are a few words :) ...  ";
	$binary = "\x09Example string\x0A";
	$hello  = "Hello World";
	var_dump($text, $binary, $hello);

	print "\n";

	$trimmed = trim($text);
	//echo $trimmed;
	//var_dump($trimmed);

	$trimmed = trim($text, "\t.");
	echo $trimmed;
	//var_dump($trimmed);

	$trimmed = trim($hello, "Hdle");
	var_dump($trimmed);

	$trimmed = trim($hello, 'HdWr');
	var_dump($trimmed);

	// trim the ASCII control characters at the beginning and end of $binary
	// (from 0 to 31 inclusive)
	$clean = trim($binary, "\x00..\x1F");
	var_dump($clean);





/*
Nowdocs are to single-quoted strings what heredocs are to double-quoted strings. A nowdoc is specified similarly to a heredoc, but no parsing is done inside a nowdoc. The construct is ideal for embedding PHP code or other large blocks of text without the need for escaping
*/

//Heredoc
echo <<<"FOOBAR"
Hello World!
//below line will give parse error
I am printing some {$foo->bar[1]}.
FOOBAR;

//Nowdoc
echo <<<'FOOBAR'
Hello World!
Now, I am printing some {$foo->bar[1]}.
FOOBAR;




?>
	</body>
</html>
