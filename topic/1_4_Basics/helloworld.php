<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
	<head>
		<title>Hello World</title>
	</head>
	<body>
		<?php
		// single-line comments are like this
		# or like this (less common)
		/* double-line comments are written
		   like this, so that you can keep typing
			 and typing
		*/
    ?>
	<?php //on

		//and

	//off ?>


URL: http://able2know.org/topic/71607-1
		?>
		<?php echo "Hello World!"; ?><br />
		<?php echo "Hello" . " World!"; ?><br />
		<?php echo 2 + 3; ?>
		<br />

// Advanced escaping using conditions
    <?php if ($expression == true): ?>
      This will show if the expression is true.
    <?php else: ?>
      Otherwise this will show.
    <?php endif; ?>


	</body>
</html>
