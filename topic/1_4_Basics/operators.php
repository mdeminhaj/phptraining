
<?php

/*Arithmetic operators:
+ Addition ex: $j + 1
- Subtraction ex:$j - 6
* Multiplication ex:$j * 11
/ Division ex:$j / 4
% Modulus (division remainder) ex:$j % 9
++ Increment ex:++$j
-- Decrement ex:--$j
*/

/*Assignment operators:
Operator Example Equivalent to
= 		$j =15 		$j = 15
+= 		$j +=5 		$j = $j + 5
-= 		$j -=3 		$j = $j - 3
*= 		$j *=8 		$j = $j * 8
/= 		$j /=16 	$j = $j / 16
.= 		$j .=$k 	$j = $j . $k
%= 		$j %=4 		$j = $j % 4*/


/*Comparison operators:
Operator Description 		Example
== 		Is equalto 			$j ==4
!= 		Is not equalto 		$j !=21
> 		Is greater than 	$j >3
< 		Is less than 		$j <100
>= 		Is greater than or equalto $j >=15
<= 		Is less than or equalto $j <=8
*/
$hour = 13;
if ($hour > 12 && $hour < 14)
//dolunch();


/*
Logical operators:
Operator 	Description 		Example
&& 		And 					$j == 3 &&$k == 2
and 	Low-precedence and 		$j == 3 and$k == 2
|| 		Or 						$j < 5 ||$j > 10
or 		Low-precedence or 		$j < 5 or$j > 10
! 		Not 					! ($j ==$k)
xor 	Exclusive or 			$j xor$k

Note that &&is usually interchangeable with  and; the same is true for  ||and  or. But
andand orhave a lower precedence, so in some cases, you may need extra parenthe‐
ses to force the required precedence. On the other hand, there are times when  only
andor orare acceptable, as in the following statement, which uses an oroperator:

*/

$html = file_get_contents($site) or die("Cannot download from $site");

/*The most unusual of these operators is xor, which stands for  exclusive or  and returns
a  TRUEvalue if either value is  TRUE, but a  FALSEvalue if both inputs are  TRUEor both
inputs are  FALSE.*/


$ingredient = $ammonia xor $bleach;

/*In the example, if either $ammoniaor $bleachis TRUE,  $ingredient  will also be set to
TRUE. But if both are TRUEor both are FALSE, $ingredientwill be set to FALSE.*/

//Strings have their own operator, the period (.)

//<<<  operator: here-documentor  heredoc

//<?php
$author = "Brian W. Kernighan";
echo <<<_END
Debugging is twice as hard as writing the code in the first place.
Therefore, if you write the code as cleverly as possible, you are,
by definition, not smart enough to debug it.
- $author.
_END;


/*It is important to remember that the closing  _END;  tag  mustappear right at the start
of a new line and it must be the  onlything on that line—not even a comment is
allowed to be added after it (nor even a single space).
*/



// ternary conditional operator:
//If the value of the first subexpression is TRUE (non-zero),
//then the second subexpression is evaluated, and that is the result of the conditional expression.
//Otherwise, the third subexpression is evaluated, and that is the value.

//$first ? $second : $third;






function double($i)
{
    return $i*2;
}
$b = $a = 5;        /* assign the value five into the variable $a and $b */
$c = $a++;          /* post-increment, assign original value of $a
                       (5) to $c */
$e = $d = ++$b;     /* pre-increment, assign the incremented value of
                       $b (6) to $d and $e */

/* at this point, both $d and $e are equal to 6 */

$f = double($d++);  /* assign twice the value of $d before
                       the increment, 2*6 = 12 to $f */
$g = double(++$e);  /* assign twice the value of $e after
                       the increment, 2*7 = 14 to $g */
$h = $g += 10;      /* first, $g is incremented by 10 and ends with the
                       value of 24. the value of the assignment (24) is
                       then assigned into $h, and $h ends with the value
                       of 24 as well. */


//Type Operators

class MyClass
{
}

class NotMyClass
{
}
$a = new MyClass;

var_dump($a instanceof MyClass);//bool(true)

var_dump($a instanceof NotMyClass);//bool(false)



?>
