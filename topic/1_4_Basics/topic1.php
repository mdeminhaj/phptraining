<?php 
	//echo doesn't return a value,faster than print
	// both looks like a function but they are not function
	//both of them are language construct
	echo "Hello";
	echo ("Hello");
	echo "<br />";
	
	
	print("World");
	echo "<br />";
	
	//print method can return a true/false(1/0) value
	$val = print ("How are you?");
	print("$val");
	echo "<br />";
	//echo ("$val");

	//using echo() we can print multiple values but not with print()

	echo "I"." ". "am"," ","Fine";
	//print "really", "DO you?";
 ?>