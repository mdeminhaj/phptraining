<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Title Page</title>

	</head>
	<body>
		<h1 class="text-center">Hello World</h1>

    //calling a function from within the same function, called recursive
     <?php
     function recursion($a)
     {
         if ($a < 20) {
             echo "$a\n";
             recursion($a + 1);
         }
     }

     echo recursion(5);
     ?>


    	</body>
    </html>
