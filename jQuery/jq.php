<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Title Page</title>

		<!-- Bootstrap CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<h1 id="p1" class="text-center" data-text="text">Hello I am <span id="jq">Javascript</span></h1>
		
		
		<div class="container">
			<button type="button" class="btn2">clickme</button>
		<button type="button" class="btn btn-danger">button</button>
		</div>
		<br>
		

		<div class="container">
			<div class="panel panel-success">
				  <div class="panel-heading">
						<h3 class="panel-title">Panel title</h3>
				  </div>
				  <div class="panel-body">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				  </div>
				  
			</div>
			<button type="button" class="btn btn-default">toggle panel</button>
			
		</div>
		
	
		<div class="container">
			<p>If you click on me, I will disappear.</p>
			<p>Click me away!</p>
			<p >Click me too!</p>
		</div>

		<div class="container">
			<div class="div1" style="width:80px;height:80px;background-color:red;"></div><br>
			<div class="div1" style="width:80px;height:80px;background-color:green;"></div><br>
			<div class="div1" style="width:80px;height:80px;background-color:blue;"></div>
			<button class="fadein">Click to fade in boxes</button><br><br>
		</div>



		<div class="container">
			<div class="panel panel-primary">
				  <div class="panel-heading">
						<h3 class="panel-title2">Panel title</h3>
				  </div>
				  <div class="panel-body2">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				  </div>
			</div>
		</div>
		<br><br>

		<div class="container">
			<div class="divanimate"style="background:#98bf21;height:100px;width:100px;position:relative;">JQuery</div>
			<button class="animate">Start Animation</button>
		</div>

		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="js/bootstrap.min.js"></script>

		<script type="text/javascript">

		//Basic syntax is: $("selector").action()

		jQuery(document).ready(function($) {
			
			//$(".panel-body").slideUp(1500);
			//$(".panel-body").toggle(1000);
			$(".panel-body").css({
				color: 'red',
			});
			

			

			
			$(".btn-default").on('click', function() {

				$(".panel-body").slideUp(1500).slideDown(3000);
				$("#jq").html('JQuery');
				$(".panel-body").html('You Just Changed ME');		
				});



			$(".btn2").on('click', function() {
				$(".btn-danger").fadeToggle('400');
				});



			//DRY Example
			$("p").click(function(){
        	$(this).hide();
    			});

			$(".btn-danger").on('click', function() {
				$("p").show('slow');
				});



			 /*$("#p1").mouseenter(function(){
        	alert("You entered p1!");
    		});*/


			$(".div1").click(function(){
        		$(this).fadeTo("slow", 0.1111);
        	
    			});

    		  $(".fadein").click(function(){
		      
		        $(".div1").fadeOut(3000);

		       	});

    		   $(".fadein").click(function(){
        
        		$(".div1").fadeIn(3000);
    			});


    		   	//panel-body2
    		   	$(".panel-body2").css({display:'none'});
    		   	$(".panel-title2").click(function(){
			    $(".panel-body2").slideDown(3000);
				});


    		   //Animation

    		    $(".animate").click(function(){
        			//$(".divanimate").animate({left: '250px'});
        			$(".divanimate").animate({
    				left: '250px',
			        opacity: '0.5',
			        height: '150px',
			        width: '+=50px',

			    	}).animate({fontSize: '3em'}, "slow");
			    	//.animate({width: '100px', opacity: '0.8'}, "slow");
    			});

    			
    		    
		});


		</script>


	</body>

</html>