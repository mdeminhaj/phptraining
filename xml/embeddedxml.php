<?php
   $data =
   "<?xml version='1.0' encoding='UTF-8'?>
   
   <note>
      <Course>PHP</Course>
      <Subject>XMLd</Subject>
      <Company>GT</Company>
      <Price>000</Price>
   </note>";
   
   $xml=simplexml_load_string($data) or die("Error: Cannot create object");
?>



<!-- We can also call a xml data file as shown below and it produces the same result as shown above − -->

<?php
   //$xml=simplexml_load_file("data") or die("Error: Cannot create object");
   //print_r($xml);
?>
<html>
   <head>
      <body>
         
         <?php
           echo $xml->Course;
         ?>
      
      </body>
   </head>
</html>