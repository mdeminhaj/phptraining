<?php echo '<?xml version="1.0" encoding="UTF-8"?>' ?>

<!-- rss: The global container.
channel: A distributing channel. It has several descriptive tags and holds one or several items. 
title: The title of the channel. Should contains the name.
link: URL of the website that provides this channel.
description: Summary of what the provider is.
one item tag at least, for the content.
 -->

<rss version="2.0">
<channel>
  <TITLE>Functional Blog</TITLE>
  <link>http://blog.com/</link>
  <description>RSS FEED</description>
  <image>
      <url>http://blog.com/abc.gif</url>
      <link>http://blog.com/index.php</link>
  </image>
  <item>
      <title>News  of today</title>
      <link>http://blog.com/en-xml-rss.html</link>
      <description>All you need to know about RSS</description>
  </item>
  <item>
      <title>News of tomorrows</title>
      <link>http://blog.com//en-xml-rdf.html</link>
      <description>And now, all about RDF</description>
    </item>
</channel>
</rss>